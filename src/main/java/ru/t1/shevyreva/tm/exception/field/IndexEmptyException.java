package ru.t1.shevyreva.tm.exception.field;

public final class IndexEmptyException extends AbsrtactFieldException {

    public IndexEmptyException() {
        super("Error! Index is incorrect!");
    }

}
