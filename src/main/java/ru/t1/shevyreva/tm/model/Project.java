package ru.t1.shevyreva.tm.model;

import ru.t1.shevyreva.tm.api.model.IWBS;
import ru.t1.shevyreva.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

public final class Project implements IWBS {

    private String id = UUID.randomUUID().toString();

    private String name = "";

    private String description = "";

    private Status status = Status.NON_STARTED;

    private Date created = new Date();

    public Project() {

    }

    public Project(String name, Status status) {
        this.name = name;
        this.status = status;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(final Status status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @Override
    public Date getCreated() {
        return created;
    }

    public void setDate(Date created) {
        this.created = created;
    }

}
