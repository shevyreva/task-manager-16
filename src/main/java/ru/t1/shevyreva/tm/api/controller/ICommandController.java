package ru.t1.shevyreva.tm.api.controller;

public interface ICommandController {

    void showAbout();

    void showVersion();

    void showInfo();

    void showCommand();

    void showArgument();

    void showHelp();

    String showWelcome();

    String showShutdown();

}
