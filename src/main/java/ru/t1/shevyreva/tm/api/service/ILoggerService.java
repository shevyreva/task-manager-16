package ru.t1.shevyreva.tm.api.service;

public interface ILoggerService {

    void debug(String message);

    void error(Exception e);

    void info(String message);

    void command(String message);

}
